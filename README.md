# ByteDanceMasters

#### 介绍
仿`win11`项目，前端：react，后端：koa

#### 拉去自己分支项目

~~~
$ git clone -b 自己分支名 https://gitee.com/kangshengshuo/byte-dance-masters.git
~~~

#### 提交流程

~~~
$ git pull // 更新代码
$ git add .
$ git commit -m "" // 提交代码 一般feat: 
$ git push // 一定切换自己分支
~~~

#### 提交规范

~~~
'feat', // 新功能 feature
'fix', // 修复 bug
'docs', // 文档注释
'style', // 代码格式(不影响代码运行的变动)
'refactor', // 重构(既不增加新功能，也不是修复bug)
'perf', // 性能优化
'test', // 增加测试
'chore', // 构建过程或辅助工具的变动
'revert', // 回退
'build' // 打包
~~~

#### 合并到主分支代码并提交

- 先从自己分支切换到master分支
~~~
$ git checkout master
~~~
- 合并到主分支
~~~
$ git merge 自己分支名
~~~
- 提交代码
~~~
$ git push
~~~

注意： 开发切换到自己分支，合并需要切到master分支
#### 前端

~~~
$ yarn
$ yarn dev
~~~

#### 后端

~~~
$ yarn
$ yarn dev
~~~

数据库用的easy-db比mysql包操作比较简单：

[npm包文档地址](https://www.npmjs.com/package/easy-mydb)

[csdn文档地址](https://blog.csdn.net/qq_34696597/article/details/103641553?ops_request_misc=&request_id=&biz_id=102&utm_term=easy-mysql%20npm%E5%8C%85&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-0-103641553.142^v35^down_rank,185^v2^control&spm=1018.2226.3001.4187)
