### vite中安装less，sass等css预处理器

直接
~~~
yarn add xxx
~~~

**实例**：

css:
~~~scss
.kang {
    .cc {
        color: red;
    }
}
~~~

jsx: 
~~~jsx

import  "./index.scss"
const BottomBar = () => {

    return (
        <div className="kang">
            <div className="cc">111</div>
        </div>
    )
}

export default BottomBar
~~~
