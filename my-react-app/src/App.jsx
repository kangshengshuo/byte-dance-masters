import { createContext, useReducer } from "react";
import MyRouter from "@/router";
import inintialValue from "@/store";
import { reducer } from "@/store/reducer";

export const ReducerContext = createContext(null);
const App = () => {
  const [state, dispatch] = useReducer(reducer, inintialValue);
  return (
    <div className="App">
      <ReducerContext.Provider value={{ state, dispatch }}>
        <MyRouter />
      </ReducerContext.Provider>
    </div>
  );
};

export default App;
