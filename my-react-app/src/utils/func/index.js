export const getCurrentTime = () => {
  const weekdayMap = ["日", "一", "二", "三", "四", "五", "六"];
  var date = new Date();
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  month = month < 10 ? "0" + month : month;
  var day = date.getDate();
  var week = weekdayMap[date.getDay()];
  var hour = date.getHours();
  hour = hour < 10 ? "0" + hour : hour;
  var minute = date.getMinutes();
  minute = minute < 10 ? "0" + minute : minute;
  var second = date.getMilliseconds();
  second = second < 10 ? "0" + second : second;
  return {
    year,
    hour,
    minute,
    month,
    day,
    week,
    second,
  };
};

function isChinese(temp) {
  var re = /[^\u4E00-\u9FA5]/;
  if (re.test(temp)) return false;
  return true;
}

export const getRows = (str) => {
  let sum = 0;
  for (let i = 0; i < str.length; i++) {
    if (isChinese(str[i])) {
      sum += 2;
    } else {
      sum += 1;
    }
  }
  return sum % 8 ? parseInt(sum / 8) + 1 : parseInt(sum / 8);
};
