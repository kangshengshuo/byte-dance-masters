const safeJsonParse = (str) => {
  try {
    const obj = JSON.parse(str);
    return obj;
  } catch (err) {
    return null;
  }
};

export default safeJsonParse;
