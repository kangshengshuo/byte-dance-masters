
/**
 * 服务器执行请求失败的Error
 */
export default class ExecuteError extends Error {
  /**
   * @param msg 要告知用户的错误信息
   * @param res 服务器返回的JSON
   */
  constructor(msg, res) {
    super('' + msg);
    this.res = res;
    this.status = res?.status || -1;
  }

  valueOf() {
    return {
      message: this.message,
      res: this.res,
      status: this.status,
    };
  }
}
