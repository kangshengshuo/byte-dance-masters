import { useEffect, useMemo, useState } from "react";

export default function useKeysboard() {
  const [keys, setKeys] = useState([]);
  const [isUp, setIsUp] = useState(false);
  const [isDown, setIsDown] = useState(false);
  const [isEnter, setIsEnter] = useState(false)

  document.onkeydown = (e) => {
    if (!keys.length && e.key === "ArrowUp") setIsUp(true);

    if (!keys.length && e.key === "ArrowDown") setIsDown(true);

    if(!keys.length && e.key === 'Enter') setIsEnter(true)

    if (!keys.includes(e.key)) {
      if (keys.includes("Control") || keys.includes("Shift")) {
        setKeys(keys.concat(e.key));
      } else {
        setKeys([e.key]);
      }
    }
  };
  document.onkeyup = (e) => {
    const index = keys.indexOf(e.key);
    keys.splice(index, 1);
    setKeys(keys);
    setIsUp(false);
    setIsDown(false)
    setIsEnter(false)
  };
  useEffect(() => {
    return () => {
      document.onkeyup = null;
      document.onkeydown = null;
    };
  }, []);

  return {
    keys,
    isUp,
    isDown,
    isEnter
  };
}
