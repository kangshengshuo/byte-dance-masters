import { useState, useEffect} from "react";

export const useMouse = () => {

    const [mouseX, setMouseX] = useState(0)
    const [mouseY, setMouseY] = useState(0)

    useEffect(() => {
        document.onmousemove = e => {
            const { clientX, clientY } = e
            setMouseX(clientX)
            setMouseY(clientY)
        }
        return () => {
            document.onmousemove = null
        };
    }, []);

    return {
        mouseX,
        mouseY
    }
}