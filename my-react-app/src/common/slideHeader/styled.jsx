import styled from "styled-components";

export const IframeHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${(props) => props.color};
  width: 100%;
  height: 30px;
  background-color: #fff;
  cursor: move;
`;

export const IframeIcon = styled.span`
  width: 100px;
  height: 20px;
  margin-left: 5px;
`;

export const OptionIcon = styled.img`
  width: 20px;
  height: 20px;
`;

export const IframeLi = styled.div`
  width: 100%;
  height: ${(props) => props.height - 30}px;
`;
