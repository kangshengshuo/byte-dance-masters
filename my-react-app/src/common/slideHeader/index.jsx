
import { useRef, useState, useEffect } from "react";
import {
  IframeHeader,
  IframeIcon,
  OptionIcon,
  IframeLi,
} from "./styled";

export default function SlideHeader({ setFatherPoint, fullScreen, scale, title }) {

  const iframeRef = useRef(null)

  const [diffPonit, setDiffPoint] = useState({ x: 0, y: 0 })
  const [leftDown, setLeftDown] = useState(false)

  const handleMouseDown = (e) => {
    setLeftDown(true)
    const { clientX, clientY } = e
    const { left, top } = iframeRef.current.getBoundingClientRect()
    setDiffPoint({
      x: clientX - left,
      y: clientY - top
    })
  }

  const handleMouseMove = (e) => {
    if (!leftDown) return;
    const { clientX, clientY } = e;
    setFatherPoint({
      x: clientX - diffPonit.x,
      y: clientY - diffPonit.y
    })
  }
  const handleMouseUp = e => {
    setLeftDown(false)
  }

  useEffect(() => {
    if (leftDown) {
      document.onmousemove = handleMouseMove
      document.onmouseup = handleMouseUp
    } else {
      document.onmousemove = null
      document.onmouseup = null
    }
    return () => {
      document.onmousemove = null
      document.onmouseup = null
    }
  }, [leftDown])


  const [isIframeMax, setIsIframeMax] = useState(false)

  return <IframeHeader
    ref={iframeRef}
    onMouseDown={handleMouseDown}
  >
    <IframeIcon>{title}</IframeIcon>
    <div className="iframe-options">
      <div
        className="iframe-option-item"
      // onClick={() => narrowHandleClick(iframe, index)}
      >
        <OptionIcon src="/pic/feature/minimize.svg" />
      </div>

      {!isIframeMax && (
        <div
          className="iframe-option-item"
          onClick={() => {
            // expandHandleClick(iframe, index);
            setIsIframeMax(true);
            fullScreen()
          }}
        >
          <OptionIcon src="/pic/feature/maximize.svg" />
        </div>
      )}

      {isIframeMax && (
        <div
          className="iframe-option-item"
          onClick={() => {
            // smallHandleClick(iframe, index);
            setIsIframeMax(false);
            scale()
          }}
        >
          <OptionIcon src="/pic/feature/exit-maximize.svg" />
        </div>
      )}

      <div
        className="iframe-option-item"
      // onClick={() => colseHandleClick(iframe, index)}
      >
        <OptionIcon src="/pic/feature/close.svg" />
      </div>
    </div>
  </IframeHeader>
}