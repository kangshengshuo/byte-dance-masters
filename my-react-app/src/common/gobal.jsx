import styled from "styled-components";

export const Container = styled.div`
    position: absolute;
    left: ${(props) => props.x ?? 0}px;
    top: ${(props) => props.y ?? 0}px;
    width: ${(props) => props.width ?? "500px"};
    height: ${(props) => props.height ?? "250px"};
    z-index: ${(props) => props.zIndex ?? 10};
    border-radius: 5px;
    overflow: hidden;
`