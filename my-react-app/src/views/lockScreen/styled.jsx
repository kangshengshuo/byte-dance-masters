import styled, { css } from 'styled-components'
export const Bcg = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  background: url('/pic/index.jpg');
  background-size: cover;
  width: 100%;
  height: 100%;
  ${props => props.isClick && css`
  filter: blur(3px);
  background-color: rgba(0, 0, 0, 0.6);
  transition: filter 1s;
  z-index: -1;
  `
  }
`
export const Time = styled.div`
  position: absolute;
  left: 30px;
  bottom: 50px;
  color: aliceblue;
  transition: color 2s;
  transition: opacity 0.3s;
  ${props => props.isClick && css`
  opacity:0
  `}
`

export const Hour = styled.div`
  font-size: 90px;
  height: 106px;
  line-height: 106px;
`

export const Datee = styled.div`
  font-size: 40px;
`

export const FormContainer = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -80%);
  width: 280px;
  height: 200px;
  z-index: 200;
`

