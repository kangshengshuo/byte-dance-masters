import React, { useState, useEffect, useContext } from "react";
import { getCurrentTime } from "@/utils/func";
import { ReducerContext } from "@/App";
import { Bcg, Time, Hour, Datee, FormContainer } from "./styled";
import { useNavigate } from "react-router-dom";
import Login from "@/components/login";
import Register from "@/components/register";

const LockScreen = () => {
  const { state, dispatch } = useContext(ReducerContext);
  const { isLogin, isRegister, isNotlockScreen, isSeelpClick } = state;
  const navigate = useNavigate();
  const [currentTime, setCurrentTime] = useState(getCurrentTime());

  useEffect(() => {
    const timer = setInterval(() => setCurrentTime(getCurrentTime()), 1000);
    return () => {
      clearInterval(timer);
    };
  }, []);

  const handleClick = () => {
    console.log("isSeelpClick:", isSeelpClick);
    if (isSeelpClick) {
      navigate("/deskTop");
      return;
    }

    dispatch({
      type: "setState",
      payload: {
        isLogin: true,
        isNotlockScreen: true,
      },
    });
  };

  return (
    <div>
      <Bcg onClick={handleClick} isClick={isNotlockScreen}>
        {currentTime?.hour ? (
          <Time isClick={isNotlockScreen}>
            <Hour>
              {currentTime.hour}:{currentTime.minute}
            </Hour>
            <Datee>
              {currentTime.month}月{currentTime.day}日，星期{currentTime.week}
            </Datee>
          </Time>
        ) : (
          ""
        )}
      </Bcg>
      {/* 登录表单 */}
      <FormContainer style={{ display: isLogin ? "block" : "none" }}>
        <Login state={state} dispatch={dispatch} />
      </FormContainer>
      {/* 注册表单 */}
      <FormContainer style={{ display: isRegister ? "block" : "none" }}>
        <Register />
      </FormContainer>
    </div>
  );
};

export default LockScreen;
