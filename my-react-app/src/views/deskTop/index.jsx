import React, { useContext, useRef, useEffect } from "react";
import fullscreen from "@/utils/fullScreen";
import BottomBar from "@/components/bottomBar";
import WinModel from "@/components/winModel";
import DeskTopCompent from "@/components/deskTopCompent";
import ModelComponent from "@/components/ModelComponent";
import { ReducerContext } from "@/App";
import { Spin } from "antd";

const Desktop = () => {
  const { state, dispatch } = useContext(ReducerContext);
  const {
    isWinModelVisiable,
    isFullScreen,
    autoHidden,
    isdeskTopLoading,
    loadingTips,
  } = state;
  const rootNode = useRef(null);
  // 隐藏菜单逻辑
  const handlderMouseDown = () => {
    dispatch({
      type: "setState",
      payload: {
        deskMenuVisible: false,
        folderMenuVisible: false,
        barMenuVisable: false,
        isWinModelVisiable: false,
        isShowUserMeun: false,
        isShowSupplyMeun: false,
      },
    });
  };

  // 桌面全屏逻辑
  const fullScreenHandler = () => {
    fullscreen.screenChange(
      () =>
        dispatch({
          type: "setState",
          payload: {
            isFullScreen: true,
          },
        }),
      () =>
        dispatch({
          type: "setState",
          payload: {
            isFullScreen: false,
          },
        })
    );
    // 显示全屏
    if (!isFullScreen) {
      fullscreen.Fullscreen(rootNode.current);
    } else {
      // 取消全屏
      fullscreen.exitFullscreen();
    }
  };

  useEffect(() => {
    rootNode.current.onmousemove = (e) => {
      // 不隐藏状态什么都不触发
      if (!autoHidden) {
        return;
      }
      // 隐藏状态下，小于60时隐藏
      if (e.clientY <= rootNode.current.clientHeight - 40) {
        dispatch({
          type: "setState",
          payload: {
            barHeight: 0,
          },
        });
        return;
      }
      // 小于5时显示
      else if (e.clientY >= rootNode.current.clientHeight - 5) {
        dispatch({
          type: "setState",
          payload: {
            barHeight: 40,
          },
        });
      }
    };
  }, [autoHidden]);

  return (
    <div
      ref={rootNode}
      onClick={handlderMouseDown}
      style={{ width: "100%", height: "100%" }}
    >
      <DeskTopCompent
        state={state}
        dispatch={dispatch}
        fullScreenHandler={fullScreenHandler}
      />
      {isWinModelVisiable && (
        <WinModel
          animation={isWinModelVisiable ? "fadeIn" : "fadeleftIn"}
          state={state}
          dispatch={dispatch}
        />
      )}
      <BottomBar state={state} dispatch={dispatch} />
      <ModelComponent state={state} dispatch={dispatch} />
      {isdeskTopLoading && <Spin tip={loadingTips}></Spin>}
    </div>
  );
};

export default Desktop;
