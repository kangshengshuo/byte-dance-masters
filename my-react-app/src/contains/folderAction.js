export const OPEN_ACTION = 1;
export const ADD_ACTION = 2;
export const DELETE_ACTION = 3;
export const UPDATE_ACTION = 4;
export const SORT_ACTION = 5;
