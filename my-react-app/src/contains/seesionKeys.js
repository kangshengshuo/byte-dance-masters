
// 用户相关
export const ACCESS_TOKEN = 'access_token'  // 登入凭证
export const UID = 'uid'                    // 用户唯一标识
export const AVATAR = 'avatar'             // 头像
export const BACKGROUND = 'background'     // 背景
export const USERNAME = 'userName'         // 名字

