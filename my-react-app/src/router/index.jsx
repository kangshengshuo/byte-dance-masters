import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import Blog from "@/views/blog";
// BrowserRouter（需要后端配置路径）, HashRouter（路径里面带'#'）
import { lazy } from "react";
import Desktop from "@/views/deskTop"
const NotFound = lazy(() => import("@/views/notFound"));
const LockScreen = lazy(() => import("@/views/lockScreen"));

const MyRouter = () => {
  return (
    <Router>
      {/* 注册路由 */}
      <Routes>
        <Route path="/" element={<Navigate to="/index" />}></Route>
        <Route path="/index" element={<LockScreen />}></Route>
        <Route path="/desktop" element={<Desktop />}></Route>
        <Route path="/blog" element={<Blog />}></Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Router>
  );
};

export default MyRouter;
