export default {
  isFolderVisiable: false,
  explorRequesId: 0,
  selectFid: 0, // 选择的fid
  explorerTitleName: "",
  filePathData: [],
  contextMeunPathData: [], // filePathData的备份,打开逻辑用到
  explorZIndex: 20,
};
