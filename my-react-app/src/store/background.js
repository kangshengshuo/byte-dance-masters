export default {
  isShowBackGround: false,
  backGroundUrl: sessionStorage.getItem("background")
    ? sessionStorage.getItem("background")
    : "/img/wallpaper/dark/img0.jpg",
};
