// import { iframeList } from "@/common/iframeList.json";
import { iframeData } from "@/common/deskIframe.json";
export default {
  iframeList: [], // 显示在桌面的data数据
  selectIframeIndex: -1,
  iframeZindex: new Array(iframeData.length).fill(20),
};
