import contextmenu from "./contextmenu";
import fileExpoler from "./fileExpoler";
import bottomBar from "./bottomBar";
import folder from "./folder";
import model from "./model";
import select from "./select";
import background from "./background";
import iframe from "./iframe";
import winModel from "./winModel";
export default {
  deskRequestId: 0,
  isLogin: false,
  isRegister: false,
  isSeelpClick: false,
  isFullScreen: false,
  isdeskTopLoading: false,
  isNotlockScreen: false,
  loadingTips: "",
  ...winModel,
  ...contextmenu,
  ...fileExpoler,
  ...bottomBar,
  ...folder,
  ...model,
  ...select,
  ...background,
  ...iframe,
};
