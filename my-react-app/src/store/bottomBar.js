import safeJsonParse from "@/utils/func/safeJsonParse";
export default {
  barHeight: 40,
  autoHidden: false, // 自动隐藏
  barIconData: safeJsonParse(sessionStorage.getItem("barIconData"))
    ? safeJsonParse(sessionStorage.getItem("barIconData"))
    : [],
};
