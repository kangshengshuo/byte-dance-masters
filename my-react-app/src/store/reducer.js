export const reducer = (state, { type = "", payload = {} }) => {
  switch (type) {
    case "setState":
      return {
        ...state,
        ...(payload || {}),
      };
    case "deskRequest":
      return {
        ...state,
        deskRequestId: ++state.deskRequestId,
      };
    case "explorRequest":
      return {
        ...state,
        explorRequesId: ++state.explorRequesId,
      };
    default:
      return {
        ...state,
      };
  }
};
