export default {
  folderData: [], // 桌面文件夹数据源数据
  folderList: [], // 资源管理器
  isRenameId: [], // 是否为重命名的id
  isStatic: false, // 是否为静态数据
  isReName: false, // 是重命名
  isNewBuild: false, // 是新建
};
