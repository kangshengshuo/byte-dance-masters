import styled from "styled-components";

export const Container = styled.div`
  transition: height 0.5s;
  width: 100%;
  font-size: 12px;
  height: ${(props) => props.barHeight}px;
  background-image: linear-gradient(160deg, #ecf7f8, #f5e9f8);
  position: fixed;
  z-index: 20;
  bottom: 0;
  overflow: hidden;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Applications = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

Applications.item = styled.div`
  width: 30px;
  height: 30px;
  text-align: center;
  margin-left: 5px;
  color: red;
  display: flex;
  justify-content: center;
  align-items: center;
  :hover {
    box-shadow: 0 0 4px 1px #999;
    border-radius: 2px;
  }
`;
