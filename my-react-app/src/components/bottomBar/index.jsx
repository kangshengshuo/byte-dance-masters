import { Container, Applications } from "./styled";
import Win from "@/assets/win8.svg";
import { useEffect, useRef } from "react";
import BarMenu from "../deskTopCompent/mouseMenu/barMenu";
import Calculagraph from "../calculagraph";
import CalendarIcon from "@/assets/calendar.svg";

const BottomBar = ({ state, dispatch }) => {
  const { barMenuVisable, barHeight, barIconData, iframeList } = state;
  const barNode = useRef(null);
  const _handerContextMenu = (e) => {
    e.stopPropagation();
    e.preventDefault();
    dispatch({
      type: "setState",
      payload: {
        deskMenuVisible: false,
        folderMenuVisible: false,
        barMenuVisable: true,
        barMenuX: e.clientX,
        barMenuY: e.clientY,
      },
    });
  };
  useEffect(() => {
    barNode.current.oncontextmenu = _handerContextMenu;
  }, []);
  const showIframe = (item) => {
    const isExist = iframeList.find((item) => item.fid === item.fid);
    if (!isExist) {
      iframeList.push(item);
    }
    dispatch({
      type: "setState",
      payload: {
        iframeList,
      },
    });
  };
  const showFolder = () => {
    dispatch({
      type: "setState",
      payload: {
        isFolderVisiable: true,
      },
    });
  };
  const handleClick = (item) => {
    switch (item.iconType) {
      case 0:
        showFolder();
        break;
      case 3: // iframe逻辑
        showIframe(item);
        break;

      default:
        break;
    }
  };
  const winHandleClick = (e) => {
    e.stopPropagation(); // 防止事件冒泡
    e.preventDefault(); // 防止默认事件
    dispatch({
      type: "setState",
      payload: {
        isWinModelVisiable: !state.isWinModelVisiable,
        isShowUserMeun: false,
        isShowSupplyMeun: false,
      },
    });
  };

  return (
    <Container ref={barNode} barHeight={barMenuVisable ? 40 : barHeight}>
      {barMenuVisable && <BarMenu state={state} dispatch={dispatch}></BarMenu>}
      <Applications>
        <Applications.item onClick={winHandleClick}>
          <Win
            width={barHeight || barMenuVisable ? 20 : barHeight}
            height={barHeight || barMenuVisable ? 20 : barHeight}
          />
        </Applications.item>
        {barIconData?.map((item, index) => (
          <Applications.item key={item.fid}>
            <img
              src={item.iconUrl}
              width={barHeight ? 20 : barHeight}
              height={barHeight ? 20 : barHeight}
              title={item.name}
              onClick={() => handleClick(item)}
            />
          </Applications.item>
        ))}
      </Applications>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <CalendarIcon
          width={barHeight || barMenuVisable ? 20 : barHeight}
          height={barHeight || barMenuVisable ? 20 : barHeight}
        />
        <Calculagraph />
      </div>
    </Container>
  );
};

export default BottomBar;
