import React, { useRef, useContext } from "react";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Form, Input, message } from "antd";
import { userRegister } from "@/api/user";
import { ReducerContext } from "@/App";


export default function Register() {
  const { dispatch } = useContext(ReducerContext);
  const registerFormRef = useRef();
  const usernameRef = useRef();

  const openSignin = () => {
    dispatch({
      type: 'setState',
      payload: {
        isLogin: true,
        isRegister: false
      }
    })
  };

  const onRegisterFormFinish = (value) => {
    userRegister({
      userName: value.username,
      password: value.password,
    })
      .then((res) => {
        if (res.status === 200) {
          message.success("Registration completed, please login.");
          openSignin();
          registerFormRef.current.setFieldsValue("");
        } else {
          message.error("Repeated username.");
          usernameRef.current.focus();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div>
      <Form
        name="normal_register"
        className="login-form"
        onFinish={onRegisterFormFinish}
        ref={registerFormRef}
      >
        <Form.Item>
          <div style={{ textAlign: 'center' }}>
            <span style={{ color: 'white', fontSize: '30px' }}>Register an account </span>
          </div>
        </Form.Item>
        <Form.Item
          name="username"
          rules={[{ required: true, message: 'Please input your Username!' }]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Username"
            style={{ backgroundColor: 'white', opacity: 0.9 }}
            ref={usernameRef} />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: 'Please input your Password!' }]}
        >
          <Input.Password
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
            style={{ backgroundColor: 'white', opacity: 0.9 }}
          />
        </Form.Item>

        <Form.Item>
          <Button type='primary'
            style={{ backgroundColor: 'rgb(47, 126, 192)', width: '280px', marginBottom: '10px' }}
            htmlType='submit'
          >
            Sign up
          </Button>
          <div style={{ fontSize: '16px' }}>
            Already have an account? <a
              style={{ color: 'rgb(47, 126, 192)', fontWeight: '600' }}
              onClick={openSignin}
            >Sign in →</a>
          </div>
        </Form.Item>
      </Form>
    </div>
  )
}
