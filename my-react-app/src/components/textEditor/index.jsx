import React, { useState } from "react";
import { Button } from "react-bootstrap";
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-text";
import "ace-builds/src-noconflict/theme-github";
import { saveAs } from "file-saver";
import { Container } from "@/common/gobal"; 
import SlideHeader from "@/common/slideHeader";

const TextEditor = () => {
  const [content, setContent] = useState("");
  const [fatherPoint, setFatherPoint] = useState({ x: 0, y: 0 })

  const handleSave = () => {
    const blob = new Blob([content], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "file.txt");
  };

  return (
    <Container x={fatherPoint.x} y={fatherPoint.y}>
      <SlideHeader title="txt编辑器" setFatherPoint={e => setFatherPoint(e)} />
      <AceEditor
        mode="text"
        theme="github"
        onChange={setContent}
        name="editor"
        editorProps={{ $blockScrolling: true }}
        value={content}
        width="800px"
        height={ 'calc(100% - 60px)' }
      />
      <Button onClick={handleSave}>Save</Button>
    </Container>
  );
};

export default TextEditor;
