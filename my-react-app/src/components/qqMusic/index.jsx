import React, {useState} from 'react';
import ReactPlayer from 'react-player';
import "./index.less"

const QQMusic = ({ url }) => {
    const [isPlaying, setIsPlaying] = useState(false);
    const [currentSong, setCurrentSong] = useState({
      title: '我曾',
      artist: '张杰',
      url: 'http://dl.stream.qqmusic.qq.com/C400001Iv2Em3t8bwn.m4a?guid=7589326178&vkey=5E73F1E268E91B10E4A8CF22617F47A60C3B12BF8941BD4A09CDAC3D44FC62266BD8D571B0F7C67BFD9B432FB03E37E60D0A6C6D60E5CD75&uin=&fromtag=120032',
      cover: 'http://p2.music.126.net/gNbAlXamNjhR2j3aOukNhg==/109951164232796511.jpg?param=130y130',
    });
  
    const handlePlayPause = () => {
      setIsPlaying(!isPlaying);
    };
  return (
    <div className="qq-music-player">
      <div className="song-info">
        <img src={currentSong.cover} alt="song cover" />
        <div>
          <h2>{currentSong.title}</h2>
          <p>{currentSong.artist}</p>
        </div>
      </div>
      <ReactPlayer
        url={currentSong.url}
        playing={isPlaying}
        controls={false}
        width="0"
        height="0"
      />
      <div className="controls">
        <button onClick={handlePlayPause}>
          {isPlaying ? '暂停' : '播放'}
        </button>
      </div>
    </div>
  );
};

export default QQMusic;
