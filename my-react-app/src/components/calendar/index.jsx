import React, { useState, useEffect } from "react";
import {
  eachDayOfInterval,
  getMonth,
  getYear,
  startOfMonth,
  endOfMonth,
  startOfWeek,
  endOfWeek,
} from "date-fns";
import LunarCalendar from "lunar-calendar";
import "./index.less";

function Calendar() {
  const [date, setDate] = useState(new Date());

  useEffect(() => {
    const intervalId = setInterval(() => {
      setDate(new Date());
    }, 60000);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  const year = getYear(date);
  const month = getMonth(date);
  const firstDayOfMonth = startOfMonth(date);
  const lastDayOfMonth = endOfMonth(date);
  const firstMondayOfMonth = startOfWeek(firstDayOfMonth, { weekStartsOn: 1 });
  const lastSundayOfMonth = endOfWeek(lastDayOfMonth, { weekStartsOn: 1 });
  const days = eachDayOfInterval({
    start: firstMondayOfMonth,
    end: lastSundayOfMonth,
  });

  function getLunarDate(day) {
    const lunarDate = LunarCalendar.solarToLunar(
      day.getFullYear(),
      day.getMonth() + 1,
      day.getDate()
    );
    return `${lunarDate.lunarMonth}月${lunarDate.lunarDay}`;
  }

  function getHoliday(day) {
    const lunarDate = LunarCalendar.solarToLunar(
      day.getFullYear(),
      day.getMonth() + 1,
      day.getDate()
    );
    if (lunarDate.lunarMonth === 12 && lunarDate.lunarDay === 30) {
      return "除夕";
    } else if (lunarDate.lunarMonth === 1 && lunarDate.lunarDay === 1) {
      return "春节";
    } else {
      return "";
    }
  }

  return (
    <div className="calendar">
      <CalendarHeader year={year} month={month} />
      <CalendarBody
        days={days}
        getLunarDate={getLunarDate}
        getHoliday={getHoliday}
      />
    </div>
  );
}

function CalendarHeader(props) {
  const { year, month } = props;

  return (
    <div className="calendar-header">
      <div className="calendar-header__month">
        {year}年{month + 1}月
      </div>
      <div className="calendar-header__week">
        <div className="calendar-header__day">周一</div>
        <div className="calendar-header__day">周二</div>
        <div className="calendar-header__day">周三</div>
        <div className="calendar-header__day">周四</div>
        <div className="calendar-header__day">周五</div>
        <div className="calendar-header__day">周六</div>
        <div className="calendar-header__day">周日</div>
      </div>
    </div>
  );
}

function CalendarBody(props) {
  const { days, getLunarDate, getHoliday } = props;

  return (
    <div className="calendar-body">
      {days.map((day) => (
        <div
          key={day.getTime()}
          className={`calendar-body__day ${
            day.getMonth() % 2 === 0 ? "even" : "odd"
          } ${
            day.getMonth() === new Date().getMonth() &&
            day.getDate() === new Date().getDate()
              ? "today"
              : ""
          }`}
        >
          <div className="calendar-body__date">{day.getDate()}</div>
          <div className="calendar-body__lunar-date">{getLunarDate(day)}</div>
          <div className="calendar-body__holiday">{getHoliday(day)}</div>
        </div>
      ))}
    </div>
  );
}

export default Calendar;
