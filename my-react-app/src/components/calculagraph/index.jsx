import React, { useState, useEffect } from "react";

function Calculagraph() {
  const [time, setTime] = useState(new Date());
  const [intervalId, setIntervalId] = useState(null);

  useEffect(() => {
    startClock();
    return () => {
      stopClock();
    };
  }, []);

  function startClock() {
    const id = setInterval(() => {
      setTime(new Date());
    }, 1000);
    setIntervalId(id);
  }

  function stopClock() {
    clearInterval(intervalId);
  }

  return (
    <div style={{ margin: '0 10px', whiteSpace: 'nowrap' }}>
      {time.toLocaleTimeString([], { hour12: true })}
    </div>
  );
}

export default Calculagraph;
