import { Modal } from "antd";

const ModelComponent = ({ state, dispatch }) => {
  const {
    folderData,
    isTipsModelVisiable,
    modelType,
    modelTitle,
    modelContent,
  } = state;

  // 重命名是否覆盖
  const newBuildrecovered = () => {
    folderData.pop();
    dispatch({
      type: "setState",
      payload: {
        folderData,
        isTipsModelVisiable: false,
      },
    });
  };

  const handleOk = (e) => {
    e.preventDefault();
    e.stopPropagation();
    switch (modelType) {
      case "newBuild": // 新建重命名
        newBuildrecovered();
        break;

      default:
        break;
    }
  };

  const handleCancel = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const fid = folderData[folderData.length - 1].fid;
    dispatch({
      type: "setState",
      payload: {
        isTipsModelVisiable: false,
        isRenameId: [fid],
      },
    });
  };

  const mouseDownListener = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };
  return (
    <div onMouseDown={mouseDownListener}>
      <Modal
        title={modelTitle}
        onOk={handleOk}
        visible={isTipsModelVisiable}
        onCancel={handleCancel}
        okText="确认"
        cancelText="取消"
      >
        <p>{modelContent}</p>
      </Modal>
    </div>
  );
};

export default ModelComponent;
