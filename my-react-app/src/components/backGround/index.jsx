import { BackGroundContainer, Header, Title, List, AllList } from "./styled";
import { Button } from "antd";
import { updateBackGroud } from "@/api/user";
import { CloseOutlined } from "@ant-design/icons";
import { getBackground } from "@/api/image";
import { useEffect } from "react";
import { useState } from "react";
import { baseURL } from "@/api";
const BackGround = ({ state, dispatch }) => {
  const { backGroundUrl } = state;
  const backgroundUrl = [
    "/img/wallpaper/dark/img0.jpg",
    "/img/wallpaper/default/img0.jpg",
    "/img/wallpaper/ThemeA/img0.jpg",
    "/img/wallpaper/ThemeA/img1.jpg",
    "/img/wallpaper/ThemeA/img2.jpg",
    "/img/wallpaper/ThemeA/img3.jpg",
    "/img/wallpaper/ThemeB/img0.jpg",
    "/img/wallpaper/ThemeB/img1.jpg",
    "/img/wallpaper/ThemeB/img2.jpg",
    "/img/wallpaper/ThemeB/img3.jpg",
    "/img/wallpaper/ThemeC/img0.jpg",
    "/img/wallpaper/ThemeC/img1.jpg",
    "/img/wallpaper/ThemeC/img2.jpg",
    "/img/wallpaper/ThemeC/img3.jpg",
    "/img/wallpaper/ThemeD/img0.jpg",
    "/img/wallpaper/ThemeD/img1.jpg",
    "/img/wallpaper/ThemeD/img2.jpg",
    "/img/wallpaper/ThemeD/img3.jpg",
  ];

  const [userBackground, setUserBackground] = useState([])

  const getBackgroundData = async () => {
    const uid = sessionStorage.getItem('uid')
    let res = await getBackground({ uid })
    setUserBackground(res.data?.map(item => baseURL + item.url))
  }
  // api 
  useEffect(() => {
    // 初始化、挂载
    getBackgroundData()
    // 销毁生命周期
    return () => {
    }
  }, [])

  const setBackGround = async (url) => {
    // 前端替换
    await sessionStorage.setItem("background", url);
    dispatch({
      type: "setState",
      payload: {
        backGroundUrl: url,
      },
    });
    const uid = sessionStorage.getItem("uid");
    await updateBackGroud({
      uid,
      background: url,
    });
  };

  return (
    <BackGroundContainer>
      <Header>
        <Title>设置背景图片</Title>
        <Button
          onClick={() => {
            dispatch({
              type: "setState",
              payload: {
                isShowBackGround: false,
              },
            });
          }}
          icon={<CloseOutlined />}
          type="text"
        ></Button>
      </Header>
      <AllList>
        {
          userBackground.length ? (
            <div>
              <div style={{ fontSize: '12px', marginLeft: "10px" }}>用户上传背景</div>
              <List>
                {
                  userBackground.map((item, index) => (
                    <List.item
                      url={item}
                      key={index}
                      isSelect={item === backGroundUrl}
                      onClick={() => setBackGround(item)}
                    ></List.item>
                  ))
                }
              </List>
              <div style={{ fontSize: '12px', marginLeft: "10px" }}>系统默认图片</div>
            </div>
          ) : null
        }

        <List>
          {backgroundUrl?.map((item, index) => (
            <List.item
              url={item}
              key={index}
              isSelect={item === backGroundUrl}
              onClick={() => setBackGround(item)}
            ></List.item>
          ))}
        </List></AllList>
    </BackGroundContainer>
  );
};

export default BackGround;
