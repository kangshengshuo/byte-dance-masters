import styled from "styled-components";
import { css } from "styled-components";

export const BackGroundContainer = styled.div`
  position: absolute;
  width: 70%;
  height: 70%;
  background-color: #fff;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  border-radius: 15px;
  overflow: hidden;
  z-index: 20;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Title = styled.div`
  margin-left: 10px;
  margin-top: 4px;
`;

export const AllList = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding-bottom: 40px;
  overflow: hidden;
  overflow-y: scroll;
`
export const List = styled.div`
  display: grid;
  width: 100%;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 10px;
  padding: 10px;
`;

List.item = styled.div`
  height: 120px;
  border-radius: 5px;
  background-image: url(${(props) => props.url});
  background-size: 100% 100%;
  background-repeat: no-repeat;
  overflow: hidden;
  ${(props) =>
    props.isSelect &&
    css`
      border: 3px solid #4c5f6b;
    `}
  :hover {
    cursor: pointer;
    border: 3px solid #999;
  }
`;
