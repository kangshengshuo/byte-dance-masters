import { Container, Bottom, Common, CommonList, CommonLi } from "./styled";
import User from "../userAvatar";
import UserSupply from "../userSupply";
import { applications } from "@/common/commonApplications.json";
import { utools } from "@/common/utools.json";
import "./win.less";

const WinModel = ({ animation, state, dispatch }) => {
  const winModelHandleClick = (e) => {
    e.stopPropagation(); // 防止事件冒泡
    dispatch({
      type: "setState",
      payload: {
        isShowUserMeun: false,
        isShowSupplyMeun: false,
      },
    });
  };
  
  const openUtools = (cmd) => {

  }

  return (
    <Container className={animation} onClick={winModelHandleClick}>
      <div className="win-title">常见应用：</div>
      <Common>
        <CommonList>
          {applications?.map((item) => (
            <CommonLi key={item.id}>
              <a href={item.href} className="win-application" target="_blank">
                <div className="win-application-img">
                  <img src={item.iconUrl} title={item.name} />
                </div>
                <div className="win-application-name">{item.name}</div>
              </a>
            </CommonLi>
          ))}
        </CommonList>
      </Common>
      <div className="win-title">实用工具：</div>
      <Common>
        <CommonList>
          {utools.map((it) => (
            <CommonLi key={it.key}>
              <div className="win-application" onClick={() => openUtools(it.key)}>
                <div className="win-application-img">
                  <img src={it.iconUrl} title={it.name} />
                </div>
                <div className="win-application-name">{it.name}</div>
              </div>
            </CommonLi>
          ))}
        </CommonList>
      </Common>
      <Bottom>
        <User state={state} dispatch={dispatch}></User>
        <UserSupply state={state} dispatch={dispatch}></UserSupply>
      </Bottom>
    </Container>
  );
};

export default WinModel;
