import styled from "styled-components";

export const Container = styled.div`
  width: 50vw;
  height: 80vh;
  border-radius: 15px;
  background-color: #ecf7f8;
  box-shadow: 1px 1px 1px #fff;
  position: fixed;
  left: 0;
  right: 0;
  bottom: 50px;
  margin: auto;
  z-index: 40;
`;

export const Bottom = styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 40px;
  border-top: 1px solid #dbdbdb;
  display: flex;
  padding: 0 20px;
  align-items: center;
  justify-content: space-between;
`;

export const Common = styled.div`
  margin: 10px;
`;

export const CommonList = styled.div`
  display: grid;
  width: 100%;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 10px;
  margin: auto;
  padding: 10px;
`;

export const CommonLi = styled.div``