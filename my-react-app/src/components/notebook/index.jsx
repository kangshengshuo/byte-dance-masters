import React, { useState } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

function Notebook() {
  const [content, setContent] = useState('');

  return (
      <ReactQuill style={{ background: '#fff', width: '400px', height: '500px' }} value={content} onChange={setContent} />
  );
}

export default Notebook;
