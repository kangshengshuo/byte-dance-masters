import { Supply, SupplyIcon, Menu } from "./styled";
import { useNavigate } from "react-router-dom";
const UserSupply = ({ state, dispatch }) => {
  const { isShowSupplyMeun } = state;
  const navigate = useNavigate();
  const sleepHandlerClick = () => {
    dispatch({
      type: "setState",
      payload: {
        isNotlockScreen: false,
        isSeelpClick: true,
        isLogin: false,
        isRegister: false,
      },
    });
    navigate("/index");
  };
  const shutDownHandlerClick = () => {
    dispatch({
      type: "setState",
      payload: {
        isSeelpClick: false,
        isLogin: false,
        isRegister: false,
        isNotlockScreen: false,
        isShowUserMeun: false,
        isWinModelVisiable: false,
        barIconData: [],
        iframeList: [],
      },
    });
    sessionStorage.clear();
    navigate("/index");
  };
  const restartHandlerClick = () => {
    dispatch({
      type: "setState",
      payload: {
        isNotlockScreen: false,
        isShowUserMeun: false,
        isSeelpClick: false,
        isWinModelVisiable: false,
        isLogin: false,
        isRegister: false,
        barIconData: [],
        iframeList: [],
        isdeskTopLoading: true,
        loadingTips: "关闭桌面应用中...",
      },
    });
    setTimeout(() => {
      dispatch({
        type: "setState",
        payload: {
          loadingTips: "重启中...",
        },
      });
      navigate("/index");
    }, 2000);
    setTimeout(() => {
      navigate("/deskTop");
    }, 4000);
    setTimeout(() => {
      dispatch({
        type: "setState",
        payload: {
          isdeskTopLoading: false,
        },
      });
    }, 6000);
  };
  const handleClick = (e) => {
    e.stopPropagation(); // 防止事件冒泡
    e.preventDefault(); // 防止默认事件
    dispatch({
      type: "setState",
      payload: {
        isShowUserMeun: false,
        isShowSupplyMeun: true,
      },
    });
  };
  return (
    <Supply onClick={handleClick}>
      <SupplyIcon src="/img/default/powerSupply.svg" title="电源" />
      {isShowSupplyMeun && (
        <Menu>
          <Menu.item onClick={sleepHandlerClick}>睡眠</Menu.item>
          <Menu.item onClick={shutDownHandlerClick}>关机</Menu.item>
          <Menu.item onClick={restartHandlerClick}>重启</Menu.item>
        </Menu>
      )}
    </Supply>
  );
};
export default UserSupply;
