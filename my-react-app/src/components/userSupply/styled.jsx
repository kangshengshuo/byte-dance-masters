import styled from "styled-components";

export const Supply = styled.div``;

export const SupplyIcon = styled.img`
  width: 20px;
  height: 20px;
`;

export const Menu = styled.div`
  position: absolute;
  background-color: #fff;
  box-shadow: 0px 0px 4px 1px #777;
  border-radius: 4px;
  right: -10px;
  top: -80px;
`;

Menu.item = styled.div`
  width: 70px;
  padding: 5px 0;
  text-align: center;
  font-size: 10px;
  :hover {
    background-color: #dbdbdb;
    cursor: pointer;
  }
`;
