import React, { useEffect, useRef, useState } from "react";
import {
  Container,
  Grid,
  Paper,
  Typography,
  TextField,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    width: 350,
    position: "absolute",
    left: (props) => props.left,
    top: (props) => props.top,
    zIndex: 100,
    padding: 0,
    borderRadius: 5
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  button: {
    margin: theme.spacing(1),
    minWidth: 48,
    height: 48,
    fontWeight: 700,
    fontSize: 24,
    lineHeight: 1,
    borderRadius: 24,
  },
  input: {
    fontSize: 36,
    fontWeight: 700,
    textAlign: "right",
    marginBottom: theme.spacing(2),
  },
  result: {
    fontSize: 48,
    fontWeight: 700,
    textAlign: "right",
    marginBottom: theme.spacing(2),
  },
}));

function Calculator() {
  const calculatorRef = useRef(null);
  const [position, setPosition] = useState({ left: 20, top: 150 });
  const [prePosition, setPrePosition] = useState({ left: 0, top: 0 });

  const classes = useStyles(position);

  const [expression, setExpression] = useState("");
  const [result, setResult] = useState("");

  const handleButtonClick = (value) => {
    setExpression((prevExpression) => prevExpression + value);
  };

  const handleClearButtonClick = () => {
    setExpression("");
    setResult("");
  };

  const handleEqualsButtonClick = () => {
    try {
      setResult(eval(expression));
    } catch (error) {
      setResult("Error");
    }
  };

  const [leftDown, setLeftDown] = useState(false);

  useEffect(() => {
    calculatorRef.current.onmousedown = (e) => {
      const { clientX, clientY } = e;
      setPrePosition({ left: clientX - position.left, top: clientY - position.top });
      setLeftDown(true);
    };
    if (leftDown) {
      document.onmousemove = (e) => {
        const { clientX, clientY } = e;
        setPosition({ left: clientX - prePosition.left, top:  clientY - prePosition.top });
      };
      document.onmouseup = (e) => {
        setLeftDown(false);
      };
    } else {
        document.onmousemove = null
        document.onmouseup = null
    }
  }, [leftDown]);

  return (
    <Container className={classes.container} innerRef={calculatorRef}>
      <div style={{ position: 'absolute', top: 0, left: 0, width: '20px', height: '20px', cursor: 'pointer' }}>
        <img style={{ width: '20px', height: '20px' }} src="/pic/feature/close.svg" alt="img" />
      </div>
      <Grid container>
        <Grid item>
          <Paper className={classes.paper}>
            <Typography variant="h5">计算器</Typography>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              value={expression}
              onChange={(event) => setExpression(event.target.value)}
            />
            <Typography style={{ color: "#ff5e84" }} variant="h6">
              {"结果: " + result}
            </Typography>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("1")}
            >
              1
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("2")}
            >
              2
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("3")}
            >
              3
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("+")}
            >
              +
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("4")}
            >
              4
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("5")}
            >
              5
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("6")}
            >
              6
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("-")}
            >
              -
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("7")}
            >
              7
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("8")}
            >
              8
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("9")}
            >
              9
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("*")}
            >
              ×
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("0")}
            >
              0
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick(".")}
            >
              .
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("/")}
            >
              ÷
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="secondary"
              onClick={handleClearButtonClick}
            >
              Clear
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("Math.sin(")}
            >
              sin
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("Math.cos(")}
            >
              cos
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleButtonClick("Math.sqrt(")}
            >
              √
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={handleEqualsButtonClick}
            >
              =
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}

export default Calculator;
