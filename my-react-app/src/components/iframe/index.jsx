import { useState } from "react";
import { useEffect } from "react";
import IframeItem from "./iframeItem";
import safeJsonParse from "@/utils/func/safeJsonParse";

const IFrame = ({ state, dispatch }) => {
  const { iframeList, iframeZindex, barIconData } = state;

  const [diffX, setdiffX] = useState(0);
  const [diffY, setdiffY] = useState(0);
  const [selectIndex, setSelectIndex] = useState(-1);
  const [leftDown, setLeftDown] = useState(false);

  const iframeOnLoadListener = (e, iframeNode, index) => {
    console.log(e);
  };

  const mouseMoveListener = (e) => {
    const { clientX, clientY } = e;
    iframeList[selectIndex].x = clientX - diffX;
    iframeList[selectIndex].y = clientY - diffY;
    dispatch({
      type: "setState",
      payload: {
        iframeList,
      },
    });
  };

  const mouseDownListener = (e, modelNode, index) => {
    const { clientX, clientY } = e;
    const { offsetLeft, offsetTop } = modelNode.current;
    setdiffX(clientX - offsetLeft);
    setdiffY(clientY - offsetTop);
    setSelectIndex(index);
    setLeftDown(true);
  };

  useEffect(() => {
    if (leftDown) {
      document.onmousemove = mouseMoveListener;
      document.onmouseup = mouseUpListener;
    } else {
      document.onmousemove = null;
      document.onmouseup = null;
    }
  }, [leftDown]);

  const mouseUpListener = (e) => {
    setdiffX(0);
    setdiffY(0);
    setLeftDown(false);
  };

  const narrowHandleClick = (iframe, index) => {
    const isExitBarIcon = barIconData.find((item) => item.fid === item.fid);
    if (!isExitBarIcon) {
      barIconData.push(iframe);
      sessionStorage.setItem("barIconData", JSON.stringify(barIconData));
    }
    console.log(barIconData);
    iframeList.splice(index, 1);
    dispatch({
      type: "setState",
      payload: {
        iframeList,
        barIconData,
      },
    });
  };

  const colseHandleClick = (iframe, index) => {
    const barIndex = barIconData.findIndex((item) => item.fid === iframe.fid);
    if (barIndex > -1) {
      barIconData.splice(barIndex, 1);
      sessionStorage.setItem("barIconData", JSON.stringify(barIconData));
    }
    iframeList.splice(index, 1);
    dispatch({
      type: "setState",
      payload: {
        iframeList,
        barIconData,
      },
    });
  };

  const expandHandleClick = async (iframe, index) => {
    var clientHeight =
      document.documentElement.clientHeight || document.body.clientHeight;
    var clientWidth =
      document.documentElement.clientWidth || document.body.clientWidth;
    const barIndex = barIconData.findIndex((item) => item.fid === iframe.fid);
    await sessionStorage.setItem(
      `${iframe.name + iframe.fid}XY`,
      JSON.stringify({
        x: iframe.x,
        y: iframe.y,
      })
    );
    iframe.x = 0;
    iframe.y = 0;
    iframe.width = clientWidth;
    iframe.height = clientHeight - 40;
    if (barIndex > -1) {
      barIconData[barIndex] = iframe;
      sessionStorage.setItem("barIconData", barIconData);
    }
    iframeList[index] = iframe;
    dispatch({
      type: "setState",
      payload: {
        iframeList,
        barIconData,
      },
    });
  };

  const smallHandleClick = (iframe, index) => {
    const barIndex = barIconData.findIndex((item) => item.fid === iframe.fid);
    const iframeStroageXY = safeJsonParse(
      sessionStorage.getItem(`${iframe.name + iframe.fid}XY`)
    );
    iframe.x = iframeStroageXY ? iframeStroageXY.x : 0;
    iframe.y = iframeStroageXY ? iframeStroageXY.y : 0;
    iframe.width = 700;
    iframe.height = 300;
    if (barIndex > -1) {
      barIconData[barIndex] = iframe;
      sessionStorage.setItem("barIconData", barIconData);
    }
    iframeList[index] = iframe;
    dispatch({
      type: "setState",
      payload: {
        iframeList,
        barIconData,
      },
    });
  };

  const selecthandlerClick = (i) => {
    iframeZindex.forEach((item, index, arr) => {
      if (i === index) {
        arr[index] = 21;
      } else {
        arr[index] = 20;
      }
    });

    dispatch({
      type: "setState",
      payload: {
        iframeZindex,
        explorZIndex: 20,
      },
    });
  };

  return (
    <>
      {iframeList?.map((item, index) => (
        <IframeItem
          state={state}
          dispatch={dispatch}
          width={item.width}
          height={item.height}
          index={index}
          key={index}
          x={item.x}
          y={item.y}
          zIndex={iframeZindex[index]}
          iframe={item}
          iframeColor={item.iframeColor}
          iconUrl={item.iconUrl}
          iframeUrl={item.iframeUrl}
          iframeOnLoadListener={iframeOnLoadListener}
          mouseDownListener={mouseDownListener}
          mouseUpListener={mouseUpListener}
          narrowHandleClick={narrowHandleClick}
          colseHandleClick={colseHandleClick}
          expandHandleClick={expandHandleClick}
          smallHandleClick={smallHandleClick}
          selecthandlerClick={selecthandlerClick}
        />
      ))}
    </>
  );
};

export default IFrame;
