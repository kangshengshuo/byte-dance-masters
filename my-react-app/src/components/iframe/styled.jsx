import styled from "styled-components";

export const IframeContent = styled.div`
  position: absolute;
  left: ${(props) => props.x}px;
  top: ${(props) => props.y}px;
  width: ${(props) => props.width}px;
  height: ${(props) => props.height}px;
  z-index: ${(props) => props.zIndex};
  border-radius: 3px;
  overflow: hidden;
  box-shadow: 0 0 5px 1px #999;
`;

export const IframeHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${(props) => props.color};
  width: 100%;
  height: 30px;
  background-color: #fff;
`;

export const IframeIcon = styled.img`
  width: 20px;
  height: 20px;
  margin-left: 5px;
`;

export const OptionIcon = styled.img`
  width: 20px;
  height: 20px;
`;

export const IframeLi = styled.div`
  width: 100%;
  height: ${(props) => props.height - 30}px;
`;

export const Iframe = styled.iframe`
  transform-origin: 0 0;
  transform: scale(.68, .68);
  width: 150%;
  height: 150%;
`;
