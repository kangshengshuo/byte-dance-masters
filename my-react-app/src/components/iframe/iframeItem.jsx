import { useState } from "react";
import { useRef } from "react";
import {
  IframeContent,
  IframeHeader,
  IframeIcon,
  OptionIcon,
  IframeLi,
  Iframe,
} from "./styled";
const IframeItem = ({
  iframe,
  width,
  height,
  x,
  y,
  index,
  zIndex,
  iframeColor,
  iconUrl,
  iframeUrl,
  iframeOnLoadListener,
  mouseDownListener,
  colseHandleClick,
  expandHandleClick,
  smallHandleClick,
  narrowHandleClick,
  selecthandlerClick
}) => {
  const modelNode = useRef(null);
  const iframeNode = useRef(null);
  const [isIframeMax, setIsIframeMax] = useState(false);
  return (
    <IframeContent
      width={width}
      height={height}
      x={x}
      y={y}
      zIndex={zIndex}
      ref={modelNode}
      onClick={() => selecthandlerClick(index)}
    >
      <IframeHeader
        color={iframeColor}
        onMouseDown={(e) => mouseDownListener(e, modelNode, index)}
      >
        <IframeIcon src={iconUrl} />
        <div className="iframe-options">
          <div
            className="iframe-option-item"
            onClick={() => narrowHandleClick(iframe, index)}
          >
            <OptionIcon src="/pic/feature/minimize.svg" />
          </div>

          {!isIframeMax && (
            <div
              className="iframe-option-item"
              onClick={() => {
                expandHandleClick(iframe, index);
                setIsIframeMax(true);
              }}
            >
              <OptionIcon src="/pic/feature/maximize.svg" />
            </div>
          )}

          {isIframeMax && (
            <div
              className="iframe-option-item"
              onClick={() => {
                smallHandleClick(iframe, index);
                setIsIframeMax(false);
              }}
            >
              <OptionIcon src="/pic/feature/exit-maximize.svg" />
            </div>
          )}

          <div
            className="iframe-option-item"
            onClick={() => colseHandleClick(iframe, index)}
          >
            <OptionIcon src="/pic/feature/close.svg" />
          </div>
        </div>
      </IframeHeader>
      <IframeLi height={height}>
        <Iframe
          ref={iframeNode}
          src={iframeUrl}
          onLoad={(e) => iframeOnLoadListener(e, iframeNode, index)}
        />
      </IframeLi>
    </IframeContent>
  );
};

export default IframeItem;
