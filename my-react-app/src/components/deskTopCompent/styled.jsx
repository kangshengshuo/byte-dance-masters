import styled from "styled-components";

export const DeskTopS = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  overflow: hidden;
  overflow-y: auto;
  background: url(${(props) => props.url});
  background-size: 100% 100%;
  background-position: center;
`;

export const FullBtn = styled.button`
  position: absolute;
  cursor: pointer;
  right: 2px;
  top: 10px;
  background-color: black;
  color: #fff;
  border-radius: 25%;
`;
