import styled from "styled-components";
import { css } from "styled-components";

export const FolderS = styled.div`
  position: absolute;
  text-align: center;
  width: 50px;
  margin: 5px;
  top: ${(props) => props.position.top}px;
  left: ${(props) => props.position.left}px;
  z-index: ${(props) => props.position.zIndex};
  ${(props) =>
    (props.isFocus || props.isHover) &&
    css`
      border: 1px dashed #fff;
      z-index: 10;
      background-color: rgba(229, 243, 255, 0.15);
    `}
`;
FolderS.icon = styled.img`
  margin: 0;
  padding: 0;
  width: 50px;
  height: 50px;
`;
export const FolderName = styled.p`
  min-width: 50px;
  max-width: 100px;
  width: auto;
  font-size: 12px;
  color: #fff;
  font-weight: 500;
  text-align: center;
  padding: 0;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  ${(props) =>
    props.isFocus &&
    css`
      overflow: visible;
      display: block;
      word-wrap: break-word;
      white-space: normal;
    `};
`;

export const FodlerInput = styled.textarea`
  display: block;
  background: #fff;
  width: 50px;
  outline: none;
  border: none;
  resize: none;
  appearance: none;
  overflow: hidden;
  text-align: center;
  :focus {
    border: none;
  }
`;
