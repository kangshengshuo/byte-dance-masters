import React from "react";
import { SelectBoxS } from "./styled";

const SelectBox = ({
  info: { startX, startY, selectBoxWidth, selectBoxHeight },
}) => {
  return (
    <SelectBoxS
      position={{ startX, startY, selectBoxWidth, selectBoxHeight }}
    ></SelectBoxS>
  );
};
export default SelectBox;
