import styled from "styled-components";

export const SelectBoxS = styled.div`
  position: absolute;
  background-color: rgba(97, 174, 238, 0.4);
  left: ${(props) => props.position.startX}px;
  top: ${(props) => props.position.startY}px;
  width: 2px;
  height: 2px;
  transform-origin: 0 0 0;
  transform: scale(
    ${(props) => props.position.selectBoxWidth / 2},
    ${(props) => props.position.selectBoxHeight / 2}
  );
`;
