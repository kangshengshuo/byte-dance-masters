import styled from "styled-components";

export const MenuS = styled.div`
  position: fixed;
  z-index: 11;
  left: ${(props) => props.deskMenuX}px;
  top: ${(props) => props.deskMenuY}px;
  display: flex;
  font-size: 12px;
`;

export const Menu = styled.div`
  width: 200px;
  height: ${(props) => props.height}px;
  background-color: #fdfdfd;
  box-shadow: 0px 0px 0px 1px #d9d9d9;
  padding: 5px 0;
  border-radius: 5%;
`;

Menu.item = styled.div`
  padding: 5px 14px;
  cursor: pointer;
  :hover {
    background-color: #f0eeee;
  }
`;

export const NewBuild = styled.div`
  width: 100px;
  height: 100px;
  margin-left: -20px;
  margin-top: 15px;
  background-color: #fff;
  box-shadow: 0px 0px 5px 1px #999;
  padding: 5px 0;
  z-index: 12;
  border-radius: 5%;
`;

NewBuild.item = styled.div`
  padding: 5px 14px;
  cursor: pointer;
  :hover {
    background-color: #f0eeee;
  }
`;

export const BackGround = styled.div`
  width: 100px;
  height: 70px;
  margin-left: -20px;
  margin-top: 105px;
  background-color: #fff;
  box-shadow: 0px 0px 5px 1px #999;
  padding: 5px 0;
  z-index: 12;
  border-radius: 5%;
`;

BackGround.item = styled.div`
  padding: 5px 14px;
  cursor: pointer;
  :hover {
    background-color: #f0eeee;
  }
`;

export const Sort = styled.div`
  width: 100px;
  height: 70px;
  margin-left: -20px;
  margin-top: 45px;
  background-color: #fff;
  box-shadow: 0px 0px 5px 1px #999;
  padding: 5px 0;
  z-index: 12;
  border-radius: 5%;
`;

Sort.item = styled.div`
  padding: 5px 14px;
  cursor: pointer;
  :hover {
    background-color: #f0eeee;
  }
`;
