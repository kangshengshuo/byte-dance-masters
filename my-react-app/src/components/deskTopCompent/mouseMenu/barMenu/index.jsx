import React from "react";
import { Menu } from "./styled";

const BarMenu = ({ state, dispatch }) => {
  const { barMenuX, barMenuY, autoHidden } = state;
  // 菜单事件处理
  const menuHandle = (e, type) => {
    dispatch({
      type: "setState",
      payload: {
        autoHidden: !autoHidden,
        barHeight: autoHidden ? 40 : 0
      },
    });
  };
  return (
    <Menu barMenuX={barMenuX} barMenuY={barMenuY - 30}>
      <Menu.item onClick={(e) => menuHandle(e, 1)}>
        {autoHidden ? "取消隐藏" : "自动隐藏"}
      </Menu.item>
    </Menu>
  );
};

export default BarMenu;
