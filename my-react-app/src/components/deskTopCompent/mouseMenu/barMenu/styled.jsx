import styled from "styled-components";

export const Menu = styled.div`
  background-color: #fdfdfd;
  box-shadow: 0px 0px 5px 1px #b0b0b0;
  position: fixed;
  width: 200px;
  padding: 5px 0;
  left: ${(props) => props.barMenuX}px;
  top: ${(props) => props.barMenuY}px;
  z-index: 11;
  border-radius: 5%;
`;
Menu.item = styled.div`
  padding: 5px 14px;
  cursor: pointer;
  :hover {
    background-color: #f0eeee;
  }
`;
