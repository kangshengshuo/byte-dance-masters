import styled from "styled-components";

export const Menu = styled.div`
  background-color: #fdfdfd;
  box-shadow: 0px 0px 0px 1px #d9d9d9;
  position: fixed;
  width: 200px;
  padding: 5px 0;
  left: ${(props) => props.folderMenuX}px;
  top: ${(props) => props.folderMenuY}px;
  z-index: 11;
  border-radius: 5%;
`;
Menu.item = styled.div`
  padding: 5px 14px;
  cursor: pointer;
  :hover {
    background-color: #f0eeee;
  }
`;
