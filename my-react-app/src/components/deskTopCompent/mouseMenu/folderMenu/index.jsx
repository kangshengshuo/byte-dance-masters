import React from "react";
import { useNavigate } from "react-router-dom";

import { Menu } from "./styled";
import { deleteFolder } from "@/api/folder";
import { DELETE_ACTION, OPEN_ACTION, UPDATE_ACTION } from "@/contains/folderAction";
import { BLOG_ICON_TYPE, FOLDER_ICON_TYPE, IFRAME_ICON_TYPE } from "@/contains/iconTypeEnum";
import { UID } from "@/contains/seesionKeys";


const FolderMenu = ({ state, dispatch }) => {
  // 菜单事件处理
  const {
    folderMenuX, folderMenuY, folderMenuSelectNum, folderData,
    contextMeunPathData, isStatic, fileIconType,
    contextMeunIframe, barIconData, iframeList, selectFid, explorerTitleName
  } = state;

  const  navigate = useNavigate()

  const deleteFolderOption = async () => {
    // 前端删除文件夹
    const index = folderData.findIndex(
      (item) => item.fid === folderMenuSelectNum
    );
    folderData.splice(index, 1);
    dispatch({
      type: "setState",
      payload: {
        folderData,
      },
    });
    // 后端删除文件夹
    const uid = parseInt(sessionStorage.getItem("uid"));
    await deleteFolder({
      uid,
      fid: folderMenuSelectNum,
    });
  };

  const openFile = () => {
    switch (fileIconType) {
      case FOLDER_ICON_TYPE:
        dispatch({
          type: "setState",
          payload: {
            isFolderVisiable: true,
            selectFid: folderMenuSelectNum,
            filePathData: contextMeunPathData,
          },
        });
        break;
      // 发现右键打开文件存name值是没有必要的，uid-fid已经是唯一了
      case BLOG_ICON_TYPE:
        const uid = sessionStorage.getItem(UID)
        navigate('/blog', { state: { uid, fid:  selectFid, name: "博客文件"} })
        break;
      case IFRAME_ICON_TYPE:
        openIframe();
        break;
      default:
        break;
    }
  };

  const openIframe = () => {
    const isExeit = iframeList.find(
      (item) => item.fid === contextMeunIframe.fid
    );
    if (isExeit) return;
    iframeList.push(contextMeunIframe);

    const barIndex = barIconData.findIndex(
      (item) => item.fid === contextMeunIframe.fid
    );
    if (barIndex === -1) {
      barIconData.push(contextMeunIframe);
      sessionStorage.setItem(
        `${contextMeunIframe.name + contextMeunIframe.fid}`,
        JSON.stringify(contextMeunIframe)
      );
    }
    dispatch({
      type: "setState",
      payload: {
        barIconData,
        iframeList,
      },
    });
  }

  const menuHandle = (type) => {
    switch (type) {
      case OPEN_ACTION:
        openFile();
        break;
      case DELETE_ACTION:
        deleteFolderOption();
        break;
      case UPDATE_ACTION:
        dispatch({
          type: "setState",
          payload: {
            isRenameId: [folderMenuSelectNum],
          },
        });
        break;
      default:
        break;
    }
  };
  return (
    <Menu folderMenuX={folderMenuX} folderMenuY={folderMenuY}>
      <Menu.item onClick={() => menuHandle(OPEN_ACTION)}>打开</Menu.item>
      {!isStatic && <Menu.item onClick={() => menuHandle(DELETE_ACTION)}>删除</Menu.item>}
      {!isStatic && <Menu.item onClick={() => menuHandle(UPDATE_ACTION)}>重命名</Menu.item>}
    </Menu>
  );
};

export default FolderMenu;
