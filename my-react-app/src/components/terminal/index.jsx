import React, {
  useRef,
  useContext,
  useEffect,
  useState,
  useCallback,
  useMemo,
} from "react";
import SlideHeader from "@/common/slideHeader";
import { Container } from "@/common/gobal";
import { USERNAME } from "@/contains/seesionKeys";
import { Input } from "antd";
import "./index.css";
import useKeysboard from "@/utils/hooks/keyboard";

const { TextArea } = Input;

export default function Terminal() {
  const [fatherPoint, setFatherPoint] = useState({ x: 0, y: 0 });
  const [preFatherPoint, setPreFatherPoint] = useState({ x: 0, y: 0 });
  const [actionRecord, setActionRecord] = useState([]);
  const [actionAllRecord, setActionAllRecord] = useState([]);
  let [recordIndex, setRecordIndex] = useState(0);

  const [value, setValue] = useState("");
  const [isFoucus, setIsFocus] = useState(false);

  const inputRef = useRef(null);
  const cmdRef = useRef(null);

  const name = sessionStorage.getItem(USERNAME);

  const { isEnter, isDown, isUp } = useKeysboard();

  const isClear = useMemo(() => value === "clear\n" || value === "\nclear", [value]);

  useEffect(() => {
    if (isFoucus && actionAllRecord.length) {
      if (recordIndex <= 0 && isUp) {
        setRecordIndex(0);
    }

      if (recordIndex > 0 && isUp) {
        setRecordIndex(--recordIndex);
        setValue(actionAllRecord[recordIndex])
      }

      if (recordIndex >= actionAllRecord.length && isDown) {
        setRecordIndex(actionAllRecord.length);
      }

      if (recordIndex < actionAllRecord.length && isDown) {
        setRecordIndex(++recordIndex);
        setValue(actionAllRecord[recordIndex])
      }
    }
  }, [isUp, isDown]);


  useEffect(() => {
    if (isEnter && value && isFoucus) {
      const newActionAllRecord = actionAllRecord.concat(value);
      setActionAllRecord(newActionAllRecord);
      setRecordIndex(newActionAllRecord.length);
    }

  }, [isEnter, isFoucus]);

  useEffect(() => {
    inputRef?.current?.focus({
      cursor: "end",
    });
  }, []);

  useEffect(() => {
    if (isEnter && isFoucus && value) {
      if (isClear) {
        setActionRecord([]);
      } else {
        setActionRecord(actionRecord.concat([value]));
      }
      setValue("");
    }
  }, [isEnter, value, isClear]);

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  const fullScreen = () => {
    setFatherPoint((pre) => {
      setPreFatherPoint(pre);
      return {
        x: 0,
        y: 0,
      };
    });
    cmdRef.current.style.width = "100%";
    cmdRef.current.style.height = "100vh";
  };

  const scale = () => {
    setFatherPoint({
      x: preFatherPoint.x,
      y: preFatherPoint.y,
    });
    cmdRef.current.style.width = "500px";
    cmdRef.current.style.height = "250px";
  };

  return (
    <Container x={fatherPoint.x} y={fatherPoint.y} ref={cmdRef} zIndex={101}>
      <SlideHeader
        title="终端"
        setFatherPoint={setFatherPoint}
        fullScreen={fullScreen}
        scale={scale}
      />
      <div
        style={{
          background: "black",
          overflow: "auto",
          height: "calc(100% - 30px)",
          color: "#fff",
          padding: "10px",
        }}
      >
        {actionRecord?.map((it, index) => (
          <div key={index} className="textWrap">
            {" "}
            <span>{name + " >  "}</span>
            <span style={{ marginLeft: "17px" }}>{it}</span>
          </div>
        ))}
        <div style={{ display: "flex" }}>
          <span style={{ lineHeight: "28px" }}>{name + " >  "}</span>
          <TextArea
            ref={inputRef}
            onFocus={() => setIsFocus(true)}
            onAbort={() => setIsFocus(false)}
            value={value}
            onChange={handleChange}
            className="input-box"
            bordered={false}
            autoSize
          />
        </div>
      </div>
    </Container>
  );
}
