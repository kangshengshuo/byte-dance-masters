import React, { useEffect, useRef, useState } from "react";
import SlideHeader from "@/common/slideHeader";
import { Container } from "@/common/gobal";
import Editor from "@monaco-editor/react";
import SideBar from "./sideBar";

export default function VsCode() {
  const [fatherPoint, setFatherPoint] = useState({ x: 0, y: 0 });
  const [preFatherPoint, setPreFatherPoint] = useState({ x: 0, y: 0 });

  const vscodeRef = useRef(null);

  const fullScreen = () => {
    setFatherPoint((pre) => {
      setPreFatherPoint(pre);
      return {
        x: 0,
        y: 0,
      };
    });
    vscodeRef.current.style.width = "100%";
    vscodeRef.current.style.height = "100vh";
  };

  const scale = () => {
    setFatherPoint({
      x: preFatherPoint.x,
      y: preFatherPoint.y,
    });
    vscodeRef.current.style.width = "75%";
    vscodeRef.current.style.height = "75%";
  };

  return (
    <Container
      x={fatherPoint.x}
      y={fatherPoint.y}
      width="75%"
      height="75%"
      ref={vscodeRef}
      zIndex={101}
    >
      <SlideHeader
        title="vscode"
        setFatherPoint={setFatherPoint}
        fullScreen={fullScreen}
        scale={scale}
      />
      <div
        style={{
          background: "#252526",
          display: "flex",
          height: "calc(100% - 30px)",
          color: "#fff",
        }}
      >
        <div style={{ width: "200px" }}>
          <SideBar />
        </div>
        <div style={{ position: "relative", background: "#1e1e1e", flex: 1 }}>
          <Editor
            height="500px"
            defaultLanguage="javascript"
            defaultValue="// some comment"
            style={{ background: "#1e1e1e" }}
          />
          <div
            style={{
              position: "absolute",
              bottom: 0,
              width: "100%",
              height: "200px",
              background: "#000",
            }}
          >
            下
          </div>
        </div>
      </div>
    </Container>
  );
}
