import { RightOutlined } from "@ant-design/icons";
import { Button } from "antd";

export const NavComponent = ({ folder, filePathData, dispatch }) => {
  const handleClick = () => {
    const index = filePathData.findIndex((item) => item.fid === folder.fid);
    dispatch({
      type: "setState",
      payload: {
        explorerTitleName: folder.name,
        selectFid: folder.fid,
        filePathData: filePathData.slice(0, index + 1),
      },
    });
  };
  return (
    <Button
      type="text"
      onClick={handleClick}
      size="small"
      icon={<RightOutlined />}
    >
      {folder.name}
    </Button>
  );
};
