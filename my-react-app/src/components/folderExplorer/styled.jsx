import styled, { css } from "styled-components";

export const FolderExplorContainer = styled.div`
  position: absolute;
  width: 80%;
  height: 60%;
  left: ${(props) => props.x}px;
  top: ${(props) => props.y}px;
`;

export const Modal = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  padding: 0 5px;
  width: 100%;
  height: 100%;
  box-shadow: 0px 0px 1px #999;
  z-index: 10;
  background-color: #fff;
`;

export const Navigator = styled.div`
  border: 1px solid rgb(214, 210, 210);
  height: 35px;
  color: red;
  padding: 5px 0 0 15px;
`;

export const Folder = styled.div`
  width: 100%;
  box-sizing: border-box;
  text-align: center;
  ${(props) =>
    props.isFocus &&
    css`
      border: 1px dashed #11a7ec;
      z-index: 10;
      background-color: rgba(82, 168, 243, 0.15);
    `}
`;

Folder.Img = styled.img`
  width: 40px;
  height: 40px;
`;

Folder.Text = styled.div`
  width: 40px;
  position: relative;
  left: 0;
  right: 0;
  margin: auto;
  overflow: hidden; /* 多出部分隐藏 */
  display: -webkit-box; /* 设置特殊属性需要的display */
  word-wrap: break-word; /* 打断单词换行 */
  text-overflow: ellipsis; /* 超出部分打点 */
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2; /* 显示最高行数 */
`;

export const FodlerInput = styled.textarea`
  display: block;
  width: 98px;
  background-color: transparent;
  outline: none;
  border: none;
  resize: none;
  appearance: none;
  overflow: hidden;
  text-align: center;
  :focus {
    border: none;
  }
`;
