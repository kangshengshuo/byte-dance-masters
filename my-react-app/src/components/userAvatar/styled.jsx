import styled, { css } from "styled-components";

export const User = styled.div`
  height: 35px;
  width: 90px;
  padding: 4px;
  border-radius: 4px;
  background-color: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow: hidden;
  :hover {
    box-shadow: 0px 0px 6px 1px #555;
    border-radius: 3px;
  }
`;

export const IconContent = styled.div`
  width: 25px;
  height: 25px;
  background-color: #e6e6e6;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
`;

export const UserName = styled.div`
  margin-left: 5px;
`;

export const Icon = styled.img`
  width: 15px;
  height: 15px;
`;

export const Menu = styled.div`
  position: absolute;
  background-color: #fff;
  box-shadow: 0px 0px 4px 1px #777;
  border-radius: 4px;
  left: 60px;
  top: -50px;
`;

Menu.item = styled.div`
  width: 70px;
  padding: 5px 0;
  text-align: center;
  font-size: 10px;
  :hover {
    background-color: #dbdbdb;
    cursor: pointer;
  }
`;
