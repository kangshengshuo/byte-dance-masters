import React, { useState, useRef, useEffect } from "react";
import { User, Menu, Icon, IconContent, UserName } from "./styled";
import { updateAvatar } from "@/api/user";
import { baseURL } from "@/api";
import { useNavigate } from "react-router-dom";
const userAvatar = ({ state, dispatch }) => {
  const { isShowUserMeun } = state;
  const [avatar, setAvatar] = useState(sessionStorage.getItem("avatar"));
  const [name, setName] = useState(sessionStorage.getItem("userName"));
  const navigate = useNavigate();
  const handleClick = (e) => {
    e.stopPropagation(); // 防止事件冒泡
    e.preventDefault(); // 防止默认事件
    dispatch({
      type: "setState",
      payload: {
        isShowUserMeun: true,
        isShowSupplyMeun: false,
      },
    });
  };
  // 上传之后
  useEffect(() => {
    sessionStorage.setItem("avatar", avatar);
  }, [avatar]);

  const logOut = () => {
    dispatch({
      type: "setState",
      payload: {
        isShowUserMeun: false,
        isLogin: false,
        isRegister: false,
        isSeelpClick: false,
        isWinModelVisiable: false,
        barIconData: [],
        iframeList: [],
        isdeskTopLoading: true,
        loadingTips: "关闭桌面应用中...",
      },
    });
    let timer1, timer2;
    clearTimeout(timer1);
    clearTimeout(timer2);
    timer1 = setTimeout(() => {
      sessionStorage.clear();
      dispatch({
        type: "setState",
        payload: {
          loadingTips: "清除缓存中...",
        },
      });
    }, 1000);
    timer2 = setTimeout(() => {
      dispatch({
        type: "setState",
        payload: {
          isdeskTopLoading: false,
          isNotlockScreen: false,
        },
      });
      navigate("/index");
    }, 3000);
  };

  const updateAvatarHandleClick = () => {
    let input = document.createElement("input");
    input.setAttribute("type", "file");
    input.setAttribute("accept", "image/*");
    input.onchange = async function () {
      let file = this.files[0];
      const uid = sessionStorage.getItem("uid");
      let formData = new FormData();
      formData.append("file", file);
      formData.append("uid", uid);
      const res = await updateAvatar(formData);
      setAvatar(res.data);
    };
    input.click(); // 模拟手动点击
  };

  return (
    <User onClick={handleClick}>
      <IconContent>
        <Icon src={avatar ? baseURL + avatar : "/pic/user.svg"} />
      </IconContent>
      <UserName>{name}</UserName>
      {isShowUserMeun && (
        <Menu>
          <Menu.item onClick={updateAvatarHandleClick}>修改头像</Menu.item>
          <Menu.item onClick={logOut}>注销</Menu.item>
        </Menu>
      )}
    </User>
  );
};

export default userAvatar;
