import axios from "axios";
import ExecuteError from "@/utils/executeError";
import { message } from "antd";

// 设置全局axios默认值
axios.defaults.timeout = 100000;
export const baseURL = "http://localhost:4000";
// export const baseURL = "http://182.61.20.203:4000";
axios.defaults.baseURL = baseURL;

// 设置请求拦截
const responseInterceptor = (resp) => {
  // 请求状态不是200，直接抛出异常
  if (resp.status !== 200) {
    throw new Error(resp.data.message);
  }
  return resp;
};

/**
 * 对axios的二次封装
 * @param option axios请求参数
 */
export default function request(option) {
  // 获取token
  let access_token = sessionStorage.getItem("access_token");
  // 新建一个axios实例
  const instance = axios.create({
    headers: {
      "Content-Type": "application/json",
      Authorization: access_token ? `Bearer ${access_token}` : "",
    },
  });
  // 设置拦截器
  instance.interceptors.response.use(responseInterceptor);
  // 执行请求并对返回结果二次处理
  return instance(option)
    .then((resp) => resp.data)
    .then((data) => {
      console.log(data);
      let resJson = data;
      // 身份过期
      if (resJson.status === 302) {
        message.error(resJson.message);
        window.location.href = "/index";
        throw new ExecuteError(resJson.msg, resJson);
      }
      // 业务处理结果不为200则抛出异常，注意200为数字类型
      if (resJson.status !== 200) {
        message.error(resJson.message);
        throw new ExecuteError(resJson.msg, resJson);
      }
      return resJson;
    })
    .catch((err) => {
      // 其他异常同一化为ExecuteError，并在控制台警告
      if (!(err instanceof ExecuteError)) {
        console.warn(err);
        message.error?.call(null, "网络拥堵，请稍后再试");
        throw new ExecuteError("网络拥堵，请稍后再试");
      }
      throw err;
    });
}

export function requestAvatar(option) {
  // 获取token
  let access_token = sessionStorage.getItem("access_token");
  // 新建一个axios实例
  const instance = axios.create({
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: access_token ? `Bearer ${access_token}` : "",
    },
  });
  // 设置拦截器
  instance.interceptors.response.use(responseInterceptor);
  // 执行请求并对返回结果二次处理
  return instance(option)
    .then((resp) => resp.data)
    .then((data) => {
      console.log(data);
      let resJson = data;
      // 身份过期
      if (resJson.status === 302) {
        message.error(resJson.message);
        window.location.href = "/index";
        throw new ExecuteError(resJson.msg, resJson);
      }
      // 业务处理结果不为200则抛出异常，注意200为数字类型
      if (resJson.status !== 200) {
        message.error(resJson.message);
        throw new ExecuteError(resJson.msg, resJson);
      }
      return resJson;
    })
    .catch((err) => {
      // 其他异常同一化为ExecuteError，并在控制台警告
      if (!(err instanceof ExecuteError)) {
        console.warn(err);
        message.error?.call(null, "网络拥堵，请稍后再试");
        throw new ExecuteError("网络拥堵，请稍后再试");
      }
      throw err;
    });
}
