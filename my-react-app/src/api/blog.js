import request from ".";

export const uploadBase64 = (data = {}) => {
  return request({
    url: "/blog/uploadBase64",
    method: "POST",
    data,
  });
};

export const updateContent = (data = {}) => {
  return request({
    url: "/blog/updateContent",
    method: "POST",
    data,
  });
};

export const getBlogContent = (data = {}) => {
  return request({
    url: "/blog/getBlogContent",
    method: "POST",
    data,
  });
};
