import request, { requestAvatar } from ".";

export const userLogin = (data = {}) => {
  return request({
    url: "/user/login",
    method: "POST",
    data,
  });
};

export const userRegister = (data = {}) => {
  return request({
    url: "/user/register",
    method: "POST",
    data,
  });
};

export const updateAvatar = (data = {}) => {
  return requestAvatar({
    url: "/user/avatar",
    method: "POST",
    data,
  });
};

export const updateBackGroud = (data = {}) => {
  return request({
    url: "/user/updatebackground",
    method: "POST",
    data,
  });
};
