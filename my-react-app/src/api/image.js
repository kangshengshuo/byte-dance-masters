import request from ".";

export const getBackground = (data = {}) => {
    return request({
        url: "/image/getBackground",
        method: "POST",
        data,
    });
};
