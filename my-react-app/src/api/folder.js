import request from ".";

export const openfolder = (data = {}) => {
  return request({
    url: "/folder/openfolder",
    method: "POST",
    data,
  });
};

export const updateName = (data = {}) => {
  return request({
    url: "/folder/name",
    method: "POST",
    data,
  });
};

export const deleteFolder = (data = {}) => {
  return request({
    url: "/folder/delete",
    method: "POST",
    data,
  });
};

export  const addFolder = (data = {}) => {
  return request({
    url: "/folder/addfolder",
    method: "POST",
    data,
  });
}
export  const sortUpdateAllFile = (data = {}) => {
  return request({
    url: "/folder/sort",
    method: "POST",
    data,
  });
}