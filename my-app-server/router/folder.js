const Router = require("koa-router");
const folderController = require("../controller/folder");

let folder = new Router();

//获取使用的文件夹信息
folder.post("/openfolder", folderController.openFolder);
//获取所有文件夹信息
folder.get("/folders", folderController.getFolders);
//删除当前文件夹
folder.post("/delete", folderController.deleteFolder);
//改变 当前文件夹到 另一个位置
folder.post("/change", folderController.changeFolder);
//修改文件夹名字
folder.post("/name", folderController.updateName);
// 新增文件夹
folder.post("/addfolder", folderController.addFolder);
// 排序
folder.post("/sort", folderController.sortUpdateAllFile);

module.exports = folder;
