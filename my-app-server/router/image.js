const Router = require("koa-router");
const imageController = require("../controller/image");

let blog = new Router();

// 获取所有背景图片
blog.post("/getBackground", imageController.getAllImageData);


module.exports = blog;
