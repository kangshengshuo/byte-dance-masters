const Router = require("koa-router");
const userController = require("../controller/user");

let user = new Router();
//登录 注册
user.post("/login", userController.login);
user.post("/register", userController.register);
//修改密码和头像
user.post("/password", userController.updatePassword);
user.post("/avatar", userController.updateAvatar);
//  修改背景
user.post("/updatebackground", userController.updateBackGroud);

module.exports = user;
