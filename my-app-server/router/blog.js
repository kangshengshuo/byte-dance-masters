const Router = require("koa-router");
const blogController = require("../controller/blog");

let blog = new Router();

// ctrl+v粘贴到页面上
blog.post("/uploadBase64", blogController.uploadBase64);
// 更新blog的内容
blog.post("/updateContent", blogController.updateContent);
// 获取blog内容
blog.post("/getBlogContent", blogController.getBlogContent);


module.exports = blog;
