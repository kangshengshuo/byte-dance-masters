const Router = require("koa-router");
const user = require("./user");
const folder = require("./folder");
const blog = require("./blog");
const image = require('./image')
// 装载所有子路由
let router = new Router();
router.use("/user", user.routes(), user.allowedMethods());
router.use("/folder", folder.routes(), folder.allowedMethods());
router.use("/blog", blog.routes(), blog.allowedMethods());
router.use('/image', image.routes(), blog.allowedMethods())
module.exports = router;
