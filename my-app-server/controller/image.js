const imageService = require("../server/image");
const { wirteJSON } = require("../utils/func");
module.exports = class blogController {
    // 更新博客
    static async getAllImageData(ctx) {
        try {
            const { uid } = ctx.request.body;
            const data = await imageService.getAllImageData(uid);
            wirteJSON(ctx, 200, "获取成功", data)
        } catch (err) {
            console.log(err);
            wirteJSON(ctx, 500, "服务器错误！", null);
        }
    }

};
