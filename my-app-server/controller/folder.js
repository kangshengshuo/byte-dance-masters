const folderService = require("../server/folder");
const { wirteJSON, listToTree, delTree } = require("../utils/func");
module.exports = class folderController {
  // 打开文件夹
  static async openFolder(ctx) {
    try {
      const { fid, uid } = ctx.request.body;
      const data = await folderService.openFolder(fid, uid);
      if (data.length) {
        wirteJSON(ctx, 200, "数据查询成功！！", data);
      } else {
        wirteJSON(ctx, 200, "数据查询成功,但是为空！", data);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }

  // 获取所有文件数据
  static async getFolders(ctx) {
    try {
      const { uid } = ctx.request.body;
      const data = await folderService.getFolders(uid);
      const mapData = listToTree(data, "fid", "fatherNode");
      if (data.length) {
        wirteJSON(ctx, 200, "数据查询成功！！", mapData);
      } else {
        wirteJSON(ctx, 404, "数据查询失败！", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }

  // 删除文件夹
  static async deleteFolder(ctx) {
    try {
      const { fid, uid } = ctx.request.body;
      let data = await folderService.getFolders(uid);
      if (!data.length) {
        wirteJSON(ctx, 404, "数据删除失败， 数据可能不存在！", null);
      }
      const tree = listToTree(data, "fid", "fatherNode"); //获得所有文件夹构成的树型数据结构
      const delId = delTree(tree, "fid", fid);
      data = await folderService.deleteFolder(delId); //删除所有相关文件夹
      if (data) {
        wirteJSON(ctx, 200, "数据删除成功！！", null);
        return;
      } else {
        wirteJSON(ctx, 404, "数据删除失败！！", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }

  // 更新文件夹
  static async changeFolder(ctx) {
    try {
      const { fid, uid, fatherNode } = ctx.request.body;
      const data = await folderService.changeFolder(fid, uid, fatherNode);
      if (data) {
        console.log(data);
        wirteJSON(ctx, 200, "数据修改成功！！", null);
      } else {
        wirteJSON(ctx, 404, "数据修改失败！", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }

  // 重命名
  static async updateName(ctx) {
    try {
      const { uid, fid, name } = ctx.request.body;
      const data = await folderService.updateName(uid, fid, name);
      if (data.affectedRows) {
        wirteJSON(ctx, 200, "数据修改成功！！", null);
      } else {
        wirteJSON(ctx, 404, "数据修改失败，uid/密码错误！！", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }

  // 添加文件
  static async addFolder(ctx) {
    try {
      const { uid, name, x, y, iconType, iconUrl, fatherNode } =
        ctx.request.body;
      await folderService.addFolder(
        uid,
        name,
        x,
        y,
        iconType,
        iconUrl,
        fatherNode
      );
      wirteJSON(ctx, 200, "添加数据成功！", null);
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }

  // 更新所有文件
  static async sortUpdateAllFile(ctx) {
    try {
      const { folderList, uid } = ctx.request.body;
      await folderService.sortUpdateAllFile(folderList, uid);
      wirteJSON(ctx, 200, "排序成功！", null);
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }
};
