const blogService = require("../server/blog");
const { wirteJSON } = require("../utils/func");
const fs = require("fs");
module.exports = class blogController {
  // 上传静态图片
  static async uploadBase64(ctx) {
    try {
      let { base64, sourceId, type, uid } = ctx.request.body; //sourceId 是时间戳
      base64 = base64.replace(/^data:image\/\w+;base64,/, "");
      let buffer = Buffer.from(base64, "base64");
      let url = ''
      switch (type) {
        case 'background':
          url = `public/uploads/bg-${sourceId}.png`
          break;
        default:
          url = `public/uploads/md-${sourceId}.png`
          break;
      }
      await fs.writeFile(url, buffer, (err) => {
        if (err) throw err;
      });
      switch (type) {
        case 'background':
          await blogService.addBackground(uid, url.slice(6))
          wirteJSON(ctx, 200, "上传成功！", { url: url.slice(6) })
          break;
        default:
          wirteJSON(ctx, 200, "上传成功！", { url: url.slice(6) });
          break;
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, "服务器错误！", null);
    }
  }
  // 更新博客
  static async updateContent(ctx) {
    try {
      const { uid, fid, content } = ctx.request.body;
      console.log(uid, fid, content);
      const data = await blogService.updateContent(uid, fid, content);
      if (data.changedRows) {
        wirteJSON(ctx, 200, "更新成功", null);
      } else {
        wirteJSON(ctx, 204, "内容未更新", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, "服务器错误！", null);
    }
  }
  //  获取博客
  static async getBlogContent(ctx) {
    try {
      const { fid, uid } = ctx.request.body;
      const data = await blogService.getBlogContent(uid, fid);
      let content = data[0].content;
      if (!content) wirteJSON(ctx, 200, "没有内容!", null);
      else wirteJSON(ctx, 200, "获取数据成功！", content);
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, "服务器错误！", null);
    }
  }
};
