const userService = require("../server/user");
const jwt = require("jsonwebtoken");
const { JWT_SECRET } = require("../config");
const { wirteJSON } = require("../utils/func");
const path = require("path");
module.exports = class userController {
  static async login(ctx) {
    try {
      const { userName, password } = ctx.request.body;
      const token = jwt.sign(
        {
          name: userName,
        },
        JWT_SECRET,
        {
          expiresIn: 60 * 60,
        }
      );
      const [data] = await userService.login(userName, password);
      console.log(data);
      if (data) {
        // 删除密码为了安全
        delete data["password"];

        wirteJSON(ctx, 200, "登入成功！", {
          ...data,
          token,
        });
      } else {
        wirteJSON(ctx, 401, "登入失败！账号密码错误！", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }

  static async register(ctx) {
    try {
      const { userName, password } = ctx.request.body;
      const data = await userService.register(userName, password);
      if (data.affectedRows) {
        wirteJSON(ctx, 200, "数据插入成功！！", null);
      } else {
        wirteJSON(ctx, 404, "数据插入失败，请试试其它账号密码！！", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }

  //修改密码
  static async updatePassword(ctx) {
    try {
      const { uid, password } = ctx.request.body;
      const data = await userService.updatePassword(uid, password);
      if (data.affectedRows) {
        wirteJSON(ctx, 200, "数据修改成功！！", null);
      } else {
        wirteJSON(ctx, 404, "数据修改失败，uid/密码错误！！", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }
  //修改头像
  static async updateAvatar(ctx) {
    try {
      const { uid } = ctx.request.body;
      const { file } = ctx.request.files;
      const fileName = path.basename(file.filepath);
      const avatar = "/uploads/" + fileName;
      const data = await userService.updateAvatar(uid, avatar);
      if (data.affectedRows) {
        wirteJSON(ctx, 200, "头像修改成功！！", avatar);
      } else {
        wirteJSON(ctx, 404, "头像修改失败，uid/头像错误！！", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, err.message, null);
    }
  }
  // 修改背景
  static async updateBackGroud(ctx) {
    try {
      const { uid, background } = ctx.request.body;
      const data = await userService.updateBackGroud(uid, background);
      if (data.affectedRows) {
        wirteJSON(ctx, 200, "背景修改成功！！", null);
      } else {
        wirteJSON(ctx, 301, "背景未修改！！", null);
      }
    } catch (err) {
      console.log(err);
      wirteJSON(ctx, 500, "服务器错误！！", null);
    }
  }
  // //修改头像
  // static async updateAvatar(ctx) {
  //   try {
  //     const { uid, avatar } = ctx.request.body;
  //     const data = await userService.updateAvatar(uid, avatar);
  //     if (data.affectedRows) {
  //       wirteJSON(ctx, 200, "数据修改成功！！", null);
  //     } else {
  //       wirteJSON(ctx, 404, "数据修改失败，uid/头像错误！！", null);
  //     }
  //   } catch (err) {
  //     console.log(err);
  //     wirteJSON(ctx, 500, err.message, null);
  //   }
  // }
};
