# 创建win11数据库

CREATE DATABASE win11;

# 查看表的字段

DESCRIBE table_name;

# 查看表的数据

SELECT * FROM table_name;


# 生成user表

CREATE TABLE user (
  uid INT PRIMARY KEY AUTO_INCREMENT,
  userName VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  avatar VARCHAR(100),
  background VARCHAR(100)
);

# 删除folder表

DROP TABLE IF EXISTS folder;

# 给ip权限

GRANT ALL PRIVILEGES ON mydatabase.* TO 'myuser'@'192.168.1.100' IDENTIFIED BY 'mypassword';

# 刷新权限

FLUSH PRIVILEGES;


# 生成folder表

CREATE TABLE folder (
    fid INT AUTO_INCREMENT PRIMARY KEY,
    uid INT,
    name VARCHAR(255) NOT NULL,
    x INT,
    y INT,
    iconType INT,
    iconUrl VARCHAR(255),
    fatherNode INT,
    content TEXT
);

# 生成image表

CREATE TABLE image (
    uid INT PRIMARY KEY,
    url VARCHAR(255)
);
