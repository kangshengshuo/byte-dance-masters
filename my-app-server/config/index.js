const { JWT_SECRET, password, database, host, port, user } = require('../env.config.json')

module.exports = {
  port: 4000,
  //数据库配置
  db: {
    host, // 主机号
    port,
    user,
    password,
    database,
    connectionLimit: 10000,
    useConnectionPooling: true,
  },
  JWT_SECRET
};
