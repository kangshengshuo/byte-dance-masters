module.exports = class userService {
  static async login(userName, password) {
    const User = require("../model/user")();
    return await User.where({
      userName,
      password,
    }).select();
  }

  static async register(userName, password) {
    const User = require("../model/user")();
    return await User.insert({
      userName,
      password,
      avatar: "https://urlify.cn/i2emE3",
      background: "/img/wallpaper/dark/img0.jpg",
    });
  }
  //修改密码
  static async updatePassword(uid, Password) {
    const User = require("../model/user")();
    return await User.where({
      uid,
    }).update({ Password });
  }

  static async updateAvatar(uid, avatar) {
    const User = require("../model/user")();
    return await User.where({
      uid,
    }).update({ avatar });
  }

  static async updateBackGroud(uid, background) {
    const User = require("../model/user")();
    return await User.where({
      uid,
    }).update({ background });
  }
};
