module.exports = class blogService {
  static async updateContent(uid, fid, content) {
    const Folder = require("../model/folder")();
    return await Folder.where({
      uid,
      fid,
    }).update({
      content,
    });
  }
  static async getBlogContent(uid, fid) {
    const Folder = require("../model/folder")();
    return await Folder.where({
      uid,
      fid,
    }).select();
  }
  static async addBackground(uid, url) {
    const Image = require("../model/image")();
    return await Image.insert({
      uid,
      url
    })
  }
};
