module.exports = class imageService {
    static async getAllImageData(uid) {
        const image = require('../model/image')();
        return await image.where({
            uid
        }).select()
    }
};
