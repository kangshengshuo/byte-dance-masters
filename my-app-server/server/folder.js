module.exports = class folderService {
  //通过前端传送的文件夹id查询子文件夹数据
  static async openFolder(fid, uid) {
    const Folder = require("../model/folder")();
    return await Folder.where({
      fatherNode: fid,
      uid,
    }).select();
  }

  static async getFolders(uid) {
    const Folder = require("../model/folder")();
    return await Folder.where({
      uid,
    }).select();
  }

  static async deleteFolder(delId) {
    const Folder = require("../model/folder")();
    return await Folder.where({
      fid: { in: delId },
    }).del();
  }

  static async changeFolder(fid, uid, fatherNode, path) {
    const Folder = require("../model/folder")();
    return await Folder.where({
      fid,
      uid,
    }).update({ fatherNode, path });
  }

  static async updateName(uid, fid, name) {
    const Folder = require("../model/folder")();
    return await Folder.where({
      fid,
      uid,
    }).update({ name });
  }

  static async addFolder(uid, name, x, y, iconType, iconUrl, fatherNode) {
    const Folder = require("../model/folder")();
    return await Folder.insert({
      uid,
      name,
      x,
      y,
      iconType,
      iconUrl,
      fatherNode,
    });
  }

  static async sortUpdateAllFile(folderList, uid) {
    const Folder = require("../model/folder")();
    let fids = [];
    let xArr = [];
    let yArr = [];
    folderList.forEach((item) => {
      fids.push(item.fid);
      xArr.push(item.x);
      yArr.push(item.y);
    });
    for (let i = 0; i < fids.length; i++) {
      await Folder.where({
        uid,
        fid: fids[i],
      }).update({
        x: xArr[i],
        y: yArr[i],
      });
    }

    return true;
  }
};
