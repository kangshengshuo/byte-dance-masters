
function wirteJSON(ctx, status, message, data = {}) {
  // User.release();
  ctx.body = {
    status,
    message,
    data,
  };
}
//删除子树：记录该子树所有节点id
function delTargetTree(tree, fid, delid) {
  delid.push(tree[fid]);
  if (tree.children) {
    for (let i in tree.children) {
      delTargetTree(tree.children[i], fid, delid);
    }
  }
  return delid;
}
//查找所在子树
function findTargetTree(tree, fid, id) {
  let delid = [];
  if (tree[fid] == id) {
    delid = delTargetTree(tree, fid, delid);
    return delid;
  }
  if (tree.children) {
    for (let i in tree.children) {
      delid = findTargetTree(tree.children[i], fid, id);
      if (delid) return delid;
    }
  }
}
//删除文件夹函数入口 分别对每个独立的树操作
function delTree(tree, fid, id) {
  for (let i in tree) {
    let delid = findTargetTree(tree[i], fid, id);
    if (delid) {
      console.log("删除文件夹：", delid);
      return delid;
    }
  }
}

function listToTree(list, id, pid) {
  let temp = {};
  let tree = [];
  list.forEach((item) => {
    temp[item[id]] = item;
  });
  for (let i in temp) {
    if (temp[i][pid] !== 0) {
      if (!temp[temp[i][pid]].children) {
        temp[temp[i][pid]].children = [];
      }
      temp[temp[i][pid]].children.push(temp[i]);
    } else {
      tree.push(temp[i]);
    }
  }
  return tree;
}

module.exports = {
  wirteJSON,
  listToTree,
  delTree,
};
