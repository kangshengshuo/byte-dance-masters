module.exports = () => {
  const EasyMydb = require("easy-mydb");
  const config = require("../config").db;
  const db = new EasyMydb(config);
  return db.model("user");
};
